# Copyright (C) 2020 Daniel Barattini.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import requests
import datetime
import json
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_0

# SHORTEST PATH:
from ryu.topology import event, switches
from ryu.topology.api import get_switch, get_link
from ryu.topology.api import get_host

import networkx as nx
from networkx.readwrite import json_graph

from ryu.lib.packet import ethernet, ether_types
from ryu.lib.packet import packet as PK


simpleswitch = "http://127.0.0.1:8090/packetin"
#simpleswitch = "http://137.204.57.106:8090/packetin"
#simpleswitch = "http://172.17.0.3:8090/packetin"
#simpleswitch = "http://sswitch:8090/packetin"

topologyupdate = "http://127.0.0.1:8070/topologyupdate" 
#topologyupdate = "http://topology:8070/topologyupdate"

topology_total_time = 0

class OfpEmitter(app_manager.RyuApp):
    """Propagate events to interested microservices.

        packet format:

        {
            'OFPXXX' : {    //XXX = event name  (ex. OFPPacketIn)
                'buffer_id'     :   int
                'data'          :   str (base64 encoded)
                'in_port'       :   int
                'reason'        :   int
                'total_len'     :   int
            },
            'dpid' :    int
        }
    """

    OFP_VERSIONS = [ofproto_v1_0.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(OfpEmitter, self).__init__(*args, **kwargs)
        self.topology_api_app = self
        self.net=nx.DiGraph()
        self.nodes = {}
        self.links = {}
        self.no_of_nodes = 0
        self.no_of_links = 0
        self.switches = []                         # self.switches = [dpid,]
        
        
        # self.swc = switches.Switches()  #remove this comment if you to activate lldp handler

    def update_topology(self):
        # UPDATE nx on simple_switch_sp_rest.py
        data_nx = json_graph.node_link_data(self.net)  # JSON serializable formatted graph in which nodes are in nodes - edges in links + other information (directionality, ... etc)
        json_nx = json.dumps(data_nx) # serialize the graph with JSON
        timestp = datetime.datetime.now()
        print('update_topology requests.post ', timestp)
        x = requests.post(topologyupdate,json=json_nx) # sending nx topology via JSON

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        start = datetime.datetime.now()
        msg = ev.msg
        # lldp packet to be ignored
        eth = PK.Packet(msg.data).get_protocol(ethernet.ethernet)
        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            print('_packet_in_handler - lldp packet ',datetime.datetime.now())
        else:
            print('_packet_in_handler start timestamp ', start)    
            datapath = msg.datapath
            dpid = datapath.id
            packet = msg.to_jsondict()
            packet['dpid'] = dpid
            print('Packet ', packet)
            stop = datetime.datetime.now()
            time_diff = (stop - start)
            ex_time = time_diff.total_seconds() * 1000
            print('_packet_in_handler time ', ex_time)

            timestp = datetime.datetime.now()
            print('_packet_in_handler start requests.post ', timestp)
            x = requests.post(simpleswitch,json=packet)
            timestp = datetime.datetime.now()
            print('_packet_in_handler end timestamp ', timestp)

    @set_ev_cls(event.EventSwitchEnter)
    def get_topology_data(self, ev):
        # get_switch() to get the list of objects Switch
        # get_link() to get the list of objects Link.
        # This objects are defined in the topology.switches file ?
        timestp = datetime.datetime.now()
        switch_list = get_switch(self.topology_api_app, None)
        self.switches = [switch.dp.id for switch in switch_list]
        
        self.net.add_nodes_from(self.switches)  
        print("switches: ", self.net.nodes)
        
        links_list = get_link(self.topology_api_app, None)
        self.links = [(link.src.dpid, link.dst.dpid, {'port': link.src.port_no}) for link in links_list]
        
        print("links with port", self.links)
        self.net.add_edges_from(self.links)  
        print("links: ", self.net.edges)
              
        self.update_topology()
        timestp2 = datetime.datetime.now()
        time_diff = (timestp2 - timestp)
        topo_time = time_diff.total_seconds() * 1000
        global topology_total_time
        topology_total_time = topology_total_time + topo_time
        print("Total time to gather all the switches and links topology data, in ms: ", topology_total_time)


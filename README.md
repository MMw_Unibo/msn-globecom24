# MSN framework - GLOBECOM 2024

MSN Microservice-based SDN Controller (based on Ryu)

## Start routing microservice
### Setting
Current Directory: ryu_microservices/routing/

Prerequisites: pip install -r requirements.txt

```bash
python3 simple_switch_rest.py
```

## Start topology microservice
### Setting
Current Directory: ryu_microservices/topology/

Prerequisites: pip install -r requirements.txt

```bash
python3 topology_learning.py
```

## Start ryu with ofp_emitter e ofctl_rest
### Setting
Current Directory: ryu_microservices/ryu_apps

Prerequisites: pip install -r requirements.txt

```bash
ryu-manager --observe-links ofp_emitter.py ofctl_rest.py
```
	
## Start mininet with 10 host e 5 switch
### Setting
Current Directory: ryu_microservices/scripts

Prerequisites: mininet

```bash
./mininet_starter.sh
```
	
! Wait some times at the startup before using mininet

## Set IP addresses

### IP address uses by ryu to communicate with simple_switch_rest and topology_learning
Set variable simpleswitch in the file ryu_app/ofp_emitter.py

### IP address uses by simple_switch_rest to communicate with ryu and topology_learning
Set variable RYU_BASE_URL in the file rest_client/simple_switch_rest.py

### IP address uses by topology_learning to communicate with ryu and simple_switch_rest
Set variable topology_learning in the file rest_client/simple_switch_rest.py

## Configure timeout per flow entry

Set in the file rest_client/simple_switch_rest.py parameters named idle_timeout (seconds) and hard_timeout (seconds) in the function build_flow(..) [default value is 0 that means no timeout]

## Execute with docker

Start the ryu container before, then the container named simple_switch_rest and then the one for topology_learning

Start the ryu container before, then the container named simple_switch_rest

## Traffic matrices

You can find the traffic matrices used in the evaluation in the traffic_matrices directory.

from flask import Flask, request, abort
import requests
import datetime
import json
import networkx as nx
from networkx.readwrite import json_graph

api = Flask(__name__)

updateagent = "http://127.0.0.1:8090/updategraph"
# updateagent = "http://sswitch:8090/updategraph"

GN = nx.DiGraph()

# to recover the graph in case of a restart of one between topology_learning and simple_switch_rest
# both microservice should have a get receiver to update the other with all the graph if needed

def print_graph():
    print("Rappresentation of the topology:")
    #TODO: Choose a method to rappresent the networkx topology graph
    #nx.draw(GN) # it doesn't work for now and it needs matplotlib

@api.route('/topologyupdate', methods=['POST'])
def post_topologyupdate():
    start1 = datetime.datetime.now()
    print('post_topologyupdate start timestamp', start1)
    
    if not request.json:
        abort(400)

    
    update = json.loads(request.json)
    # updating GN
    global GN
    tempGN = json_graph.node_link_graph(update) 
    GN.add_nodes_from(tempGN.nodes)
    GN.add_edges_from(tempGN.edges)
    
    # here we send the updated GN to the shortestpath_agent via simple_switch_rest
    timestp = datetime.datetime.now()
    print('update shortestpath_agent via simple_switch_rest requests.post ', timestp)
    x = requests.post(updateagent,json=json.dumps(update))
    
    print('post_topologyupdate - nodes: ',GN.nodes)
    print('post_topologyupdate - links: ',GN.edges)
    
    #print_graph()

    stop1 = datetime.datetime.now()
    time_diff = (stop1 - start1)
    ex_time = time_diff.total_seconds() * 1000
    print('post_topologyupdate: ', ex_time)
    print('post_topologyupdate stop timestamp', stop1)
    
    return "ACK"
    
@api.route('/hostupdate', methods=['POST'])
def post_hostupdate():
    start1 = datetime.datetime.now()
    print('post_hostupdate start timestamp', start1)
    
    if not request.json:
        abort(400)

    
    update = json.loads(request.json)
    # updating GN
    tempGN = json_graph.node_link_graph(update) #using this with (return GN[dpid][next]['port']) cause some rare type error with 'port'
    global GN
    GN.add_nodes_from(tempGN.nodes)
    GN.add_edges_from(tempGN.edges)

    print('post_hostupdate - nodes: ',GN.nodes)
    print('post_hostupdate - links: ',GN.edges)

    #print_graph()
    
    stop1 = datetime.datetime.now()
    time_diff = (stop1 - start1)
    ex_time = time_diff.total_seconds() * 1000
    print('post_hostupdate: ', ex_time)
    print('post_hostupdate stop timestamp', stop1)
    
    return "ACK"    
    
if __name__ == "__main__":
    api.run(host='0.0.0.0', port=8070)

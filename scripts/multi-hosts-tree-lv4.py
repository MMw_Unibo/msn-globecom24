from mininet.topo import Topo
class MyTopo( Topo ):
    "Simple topology example."
    def __init__( self ):
        "Create custom topo."

        # Initialize topology  #liv 2 -> 3 -> 4
        Topo.__init__( self )

        # Add hosts and switches
        Host1 = self.addHost('h1')
        Host2 = self.addHost('h2')
        Host3 = self.addHost('h3')
        Host4 = self.addHost('h4')
        Host5 = self.addHost('h5')
        Host6 = self.addHost('h6')
        Host7 = self.addHost('h7')
        Host8 = self.addHost('h8')
        Host9 = self.addHost('h9')
        Host10 = self.addHost('h10')
        
        # Core switch
        CoreSwitch = self.addSwitch('s1')

        # Distribution switches
        DistSwitch1 = self.addSwitch('s2')
        DistSwitch2 = self.addSwitch('s3')

        # Access switches
        AccessSwitch1 = self.addSwitch('s4')
        AccessSwitch2 = self.addSwitch('s5')
        AccessSwitch3 = self.addSwitch('s6')
        AccessSwitch4 = self.addSwitch('s7')

        # Add links between hosts and access switches
        self.addLink( Host1, AccessSwitch1 )
        self.addLink( Host2, AccessSwitch1 )
        self.addLink( Host3, AccessSwitch2 )
        self.addLink( Host4, AccessSwitch2 )
        self.addLink( Host5, AccessSwitch3 )
        self.addLink( Host6, AccessSwitch3 )
        self.addLink( Host7, AccessSwitch4 )
        self.addLink( Host8, AccessSwitch4 )
        self.addLink( Host9, AccessSwitch1 )
        self.addLink( Host10, AccessSwitch2 )

        # Add links between access switches and distribution switches
        self.addLink( AccessSwitch1, DistSwitch1 )
        self.addLink( AccessSwitch2, DistSwitch1 )
        self.addLink( AccessSwitch3, DistSwitch2 )
        self.addLink( AccessSwitch4, DistSwitch2 )
 

        # Add links between distribution switches and the core switch
        self.addLink( DistSwitch1, CoreSwitch )
        self.addLink( DistSwitch2, CoreSwitch )
        
        '''
        #Add an alternative route, this create a loop that generates coms problems
        AltSwitch = self.addSwitch('s8')
        self.addLink( AccessSwitch2, AltSwitch )
        self.addLink( AccessSwitch3, AltSwitch )
        '''
topos = { 'mytopo': ( lambda: MyTopo() ) }


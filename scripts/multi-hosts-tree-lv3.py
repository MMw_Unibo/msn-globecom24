from mininet.topo import Topo
class MyTopo( Topo ):
    "Tree Topology with 3 Lvls."
    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        Host1 = self.addHost('h1')
        Host2 = self.addHost('h2')
        Host3 = self.addHost('h3')
        Host4 = self.addHost('h4')
        
        # Core switch
        CoreSwitch = self.addSwitch('s1')

        # Distribution switches
        AccessSwitch1 = self.addSwitch('s2')
        AccessSwitch2 = self.addSwitch('s3')

        # Add links between hosts and access switches
        self.addLink( Host1, AccessSwitch1 )
        self.addLink( Host2, AccessSwitch1 )
        self.addLink( Host3, AccessSwitch2 )
        self.addLink( Host4, AccessSwitch2 )
 

        # Add links between access switches and the core switch
        self.addLink( AccessSwitch1, CoreSwitch )
        self.addLink( AccessSwitch2, CoreSwitch )
        
topos = { 'mytopo': ( lambda: MyTopo() ) }


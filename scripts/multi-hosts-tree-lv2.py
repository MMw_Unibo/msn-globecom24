from mininet.topo import Topo
class MyTopo( Topo ):
    "Tree Topology with 2 Lvls."
    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        Host1 = self.addHost('h1')
        Host2 = self.addHost('h2')
        
        # Core switch
        CoreSwitch = self.addSwitch('s1')

        # Add links between hosts and access switches
        self.addLink( Host1, CoreSwitch )
        self.addLink( Host2, CoreSwitch )

        
topos = { 'mytopo': ( lambda: MyTopo() ) }


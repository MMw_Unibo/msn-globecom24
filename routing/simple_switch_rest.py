# Copyright (C) 2020 Daniel Barattini.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import Flask, request, abort
from lib.packet import packet
from lib.packet import ethernet
from lib.packet import ether_types
import requests
import base64
import datetime
import json

import networkx as nx
from networkx.readwrite import json_graph

from shortestpath_agent import out_port_lookup
from shortestpath_agent import update_mac_to_port
from shortestpath_agent import update_nx_graph

from flowbuilder_agent import build_flow

from packetbuilder_agent import build_packet

api = Flask(__name__)

#TODO import from ofp
OFPP_FLOOD = 0xfffb
OFP_DEFAULT_PRIORITY = 32768
OFPFF_SEND_FLOW_REM = 1 << 0
OFP_NO_BUFFER = 0xffffffff

SEND_OUT = None

#RYU_BASE_URL = "http://137.204.57.106:8080"
RYU_BASE_URL = "http://127.0.0.1:8080"
#RYU_BASE_URL = "http://172.17.0.2:8080"
#RYU_BASE_URL = "http://ryu_middleware:8080"

topology_learning = "http://127.0.0.1:8070"
# topology_learning = "http://topology:8070"

mac_to_port = {}

hosts_discovery_time = 0

GN = nx.DiGraph()

def add_flow(flow):
    "Add a flow entry through REST"
    rest_uri = RYU_BASE_URL + "/stats/flowentry/add"

    #TODO verbose mode
    print("sending {}".format(flow))

    r = requests.post(rest_uri, json=flow)

    if r.status_code == 200:
        return True
    else:
        return False

def send_packet(pkt):
    "Send a packet to a switch through REST"
    rest_uri = RYU_BASE_URL + "/stats/sendpacket"

    start1 = datetime.datetime.now()
    print('send_packet start timestamp', start1)
    
    r = requests.post(rest_uri, json=pkt)

    if r.status_code == 200:
        return True
    else:
        return False

def extract_data(msg, event_name):
    data = msg[event_name]

    data['encoded_data'] = data['data']
    data['data'] = base64.b64decode(data['encoded_data'])
    data['dpid'] = msg['dpid']

    packet_data = packet.Packet(data['data'])
    eth = packet_data.get_protocol(ethernet.ethernet)

    if eth.ethertype == ether_types.ETH_TYPE_LLDP:
        data['is_lldp'] = True
    else:
        data['is_lldp'] = False

        data['dst'] = eth.dst
        data['src'] = eth.src

    return data

@api.route('/')
def index():
    return 'Simple Switch + Shortest Path Rest Server'

@api.route('/packetin', methods=['POST'])
def post_packetin():
    start1 = datetime.datetime.now()
    print('post_packetin start timestamp', start1)
    
    if not request.json:
        abort(400)

    start2 = datetime.datetime.now()
    data = extract_data(request.json, "OFPPacketIn")
    stop2 = datetime.datetime.now()
    time_diff = (stop2 - start2)
    ex_time = time_diff.total_seconds() * 1000
    print('extract_data: ', ex_time)
    
    if data['is_lldp']:
        # ignore lldp packet
        # now lldp packet are ignored directly in opf_emitter before the post to this ms
        return 
    
    dpid = data['dpid']
    src = data['src']
    dst = data['dst']
    in_port = data['in_port']
    buffer_id = data['buffer_id']
    encoded_data = data['encoded_data']

    update_mac_to_port(dpid, src, in_port) # updating the mac_to_port in the agent (shortestpath_agent.py)

    # adding missing hosts to the network
    
    if src not in GN:
        start_h = datetime.datetime.now()
        GN.add_node(src)
        GN.add_edges_from([(dpid,src,{'port':in_port})])
        GN.add_edge(src,dpid)
        # updating the agent
        update_nx_graph(GN)
        stop_h = datetime.datetime.now()
        time_diff = (stop_h - start_h)
        host_time = time_diff.total_seconds() * 1000
        global hosts_discovery_time
        hosts_discovery_time = hosts_discovery_time + host_time
        print('Hosts Discovery - Total Time [ms]: ',hosts_discovery_time)
        # POST to topology_learning
        UPDATE = nx.DiGraph()
        UPDATE.add_node(src)
        UPDATE.add_edges_from([(dpid,src,{'port':in_port})])
        UPDATE.add_edge(src,dpid)
        data_nx = json_graph.node_link_data(UPDATE)  # JSON serializable formatted graph in which nodes are in nodes - edges in links + other information (directionality, ... etc)
        json_nx = json.dumps(data_nx)
        rest_uri = topology_learning + "/hostupdate"
        r = requests.post(rest_uri, json=json_nx)
        print('hostupdate: updating topology_learning too')
    else:
        print(src,' already in GN')
    
    
    start_sp_agent = datetime.datetime.now()
    out_port = out_port_lookup(dpid, dst, src) # asking the agent (shortestpath_agent.py) for the out_port
    stop_sp_agent = datetime.datetime.now()
    time_diff = (stop_sp_agent - start_sp_agent)
    op_time = time_diff.total_seconds() * 1000
    print("out_port_lookup from shortestpath_agent: ", op_time)
    
    if out_port == None:
        print("packet to be dropped")
        return "ACK" # i should drop the packet because the switch is not part of the shortest path?
        
    print('out_port: ',out_port)
    if out_port != OFPP_FLOOD:
        start3 = datetime.datetime.now()
        # flow = build_flow(dpid, src, dst, in_port, out_port, buffer_id)
        flow = build_flow(dpid, src, dst, in_port, out_port, buffer_id, hard_timeout=20, idle_timeout=20) #TO = 5
        add_flow(flow)
        stop3 = datetime.datetime.now()
        time_diff = (stop3 - start3)
        fl_time = time_diff.total_seconds() * 1000
        print('build_flow: ', fl_time)
        rule_time = fl_time + op_time
        print('Switch ',dpid,': Total time to create the new rule = ',rule_time,' ms')
        
    

    msg = None
    if buffer_id == OFP_NO_BUFFER:
        msg = encoded_data

    start4 = datetime.datetime.now()
    pkt = build_packet(dpid, in_port, out_port, msg, buffer_id)
    stop4 = datetime.datetime.now()
    time_diff = (stop4 - start4)
    ex_time = time_diff.total_seconds() * 1000
    print('build_packet: ', ex_time)

    start5 = datetime.datetime.now()
    send_packet(pkt)
    stop5 = datetime.datetime.now()
    time_diff = (stop5 - start5)
    ex_time = time_diff.total_seconds() * 1000
    print('send_packet: ', ex_time)

    stop1 = datetime.datetime.now()
    time_diff = (stop1 - start1)
    ex_time = time_diff.total_seconds() * 1000
    print('post_packetin: ', ex_time)
   
    print('post_packetin stop timestamp', stop1)

    return "ACK"

@api.route('/updategraph', methods=['POST'])
def post_updategraph():
    start1 = datetime.datetime.now()
    print('post_updategraph start timestamp', start1)
    
    if not request.json:
        abort(400)

    
    update = json.loads(request.json)
    # updating GN
    GN = json_graph.node_link_graph(update)
    links = update['links']
    for link in links:
        update_mac_to_port(link['source'],link['target'],link['port']) # updating the mac_to_port in the agent (shortestpath_agent.py)
        print(link)
    
    # here we send the updated GN to the shortestpath_agent
    update_nx_graph(GN)
    
    print('post_topologyupdate - nodes: ',GN.nodes)
    print('post_topologyupdate - links: ',GN.edges)

    stop1 = datetime.datetime.now()
    time_diff = (stop1 - start1)
    ex_time = time_diff.total_seconds() * 1000
    print('post_topologyupdate: ', ex_time)
    print('post_topologyupdate stop timestamp', stop1)
    
    return "ACK"

if __name__ == "__main__":
    api.run(host='0.0.0.0', port=8090)

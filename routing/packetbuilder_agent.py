
def build_packet(dpid, in_port, out_port, data, buffer_id):
    "Build and return a packet"
    print('packetbuilder_agent: build_packet')
    pkt = {
        'dpid' : dpid,
        'buffer_id': buffer_id,
        'in_port' : in_port,
        'actions': [{"type":"OUTPUT", "port": out_port}],
        'data' : data
    }

    return pkt

import networkx as nx
import datetime
import sys

OFPP_FLOOD = 0xfffb

mac_to_port = {}

GN = nx.DiGraph()

tot_time_to_update = 0
updates_counter = 0
size = 0

def update_mac_to_port(dpid, src, in_port):
    print("shortestpath_agent: update_mac_to_port")
    mac_to_port.setdefault(dpid, {})

    # learn a mac address to avoid FLOOD next time.
    mac_to_port[dpid][src] = in_port
    
    # print(mac_to_port) # i tried and it works, so we know the agent keep the mac_to_port data

def update_nx_graph(nxgn):
    start = datetime.datetime.now()
    print("shortestpath_agent: update_nx_graph")
    tempGN = nxgn
    GN.add_nodes_from(tempGN.nodes)
    GN.add_edges_from(tempGN.edges)
    stop = datetime.datetime.now()
    
    time_diff = stop - start
    up_time = time_diff.total_seconds() * 1000
    global tot_time_to_update, updates_counter
    tot_time_to_update = tot_time_to_update + up_time
    updates_counter += 1
    
    edge_mem = sum([sys.getsizeof(e) for e in GN.edges])
    node_mem = sum([sys.getsizeof(n) for n in GN.nodes])
    graph_mem = node_mem + edge_mem
    global size
    update_size = graph_mem - size
    size = graph_mem
        
    print("nodes (hosts/switches): ",GN.nodes)
    print("links: ",GN.edges)
    
    print("shortestpath_agent: update_nx_graph - TOTAL DURATION = ", tot_time_to_update,' ms | NUMBER OF UPDATES = ', updates_counter,' | SIZE of the UPDATE = ',update_size,' bytes | SIZE of the GRAPH = ',graph_mem,' bytes')

def out_port_lookup(dpid, dst, src):
    if dst in GN:  
        try:
            print("shortestpath_agent: out_port_lookup -> SHORTEST PATH") #as before without the --observe-links. SP works only with the host connected to the same switch.
            print("between ",src," and ",dst)
            print("actual position, dpid: ",dpid)
            start = datetime.datetime.now()
            # path=nx.shortest_path(GN,src,dst, method = 'bellman-ford')
            path=nx.shortest_path(GN,src,dst, method = 'dijkstra')
            stop = datetime.datetime.now()
            time_diff = (stop - start)
            path_time = time_diff.total_seconds() * 1000
            print('SPA: path determination time in ms:  ', path_time)  
            print(path) 
            next=path[path.index(dpid)+1] # so if dpid is not in the path, there is no index -> ValueError
            print("next: ",next)
            return mac_to_port[dpid][next] # this seems to work now even with both dpid as dpid and next.
        except nx.NetworkXNoPath:
            print("No path between source - ",src," and destination - ",dst)
        except KeyError:
            print("KeyError occurred: pass to basic mac_to_port") # SOLVED ?
        except ValueError:
            print("ValueError - dpid: ",dpid," is not part of the shortest path") # So what should we return? maybe return OFPP_FLOOD ?
            return None # i need something to signal to the switch that this packet has to be dropped
    if dst in mac_to_port[dpid]:
        print("shortestpath_agent: out_port_lookup -> MAC_TO_PORT") # usefull in case "No path.." and KeyError in SP
        return mac_to_port[dpid][dst]  # this works for the in between switches comunications but follows the normal path
    else:
        print("shortestpath_agent: out_port_lookup -> OFPP_FLOOD")
        return OFPP_FLOOD
        
    

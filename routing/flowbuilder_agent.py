OFP_DEFAULT_PRIORITY = 32768
SEND_OUT = None

def build_flow(dpid, src, dst, in_port, out_port, buffer_id, cookie = 0, idle_timeout = 0, hard_timeout = 0, priority = OFP_DEFAULT_PRIORITY, flags = 1, actions = SEND_OUT):
    "Build and return a flow entry based on https://ryu.readthedocs.io/en/latest/app/ofctl_rest.html#add-a-flow-entry"
    print('flowbuilder_agent: build_flow')
    match = {'in_port': in_port, 'dl_src': src, 'dl_dst': dst}

    if actions == SEND_OUT:
        actions = [{"type":"OUTPUT", "port": out_port}]

    flow = {
        'dpid' : dpid,
        'match' : match,
        'cookie' : cookie,
        'idle_timeout' : idle_timeout,
        'hard_timeout' : hard_timeout,
        'priority' : priority,
        'flags' : flags,
        'actions': actions,
        'buffer_id': buffer_id
    }

    return flow
